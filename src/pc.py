"""

primeclassify.py

Functions to classify prime numbers

Each classification function takes an argument, `p`, which *is assumed
to be a prime number*. There also are optional arguments `tout` and
`store`. `tout` is the maximum processing time the function may take
and `store` is the maximum number of data values it can store. These
are self enforced. Some have additional optional arguments.

Each returns False (p not a member of that category), or
something other than False (p a member of that category) — usually
True, but sometimes additional information (such as, for twin primes,
which prime(s) it is a twin of), or None (could not complete test).

Rich Holmes 11/2023

"""

import sympy
from time import process_time
from math import log, isqrt, prod

primeslist=list(sympy.primerange(100000))

##### Balanced ###################################################

def balanced (p, tout=0, stor=0):
    """
In number theory, a balanced prime is a prime number with equal-sized prime gaps above and below it, so that it is equal to the arithmetic mean of the nearest primes above and below.
"""
    if p == 2:
        return False
    pp = sympy.prevprime(p)
    np = sympy.nextprime(p)
    return p-pp == np-p

##### Chen ###################################################

def chen (p, tout=0, stor=0):
    """
In mathematics, a prime number p is called a Chen prime if p + 2 is either a prime or a product of two primes (also called a semiprime).

Returns False or 1 for p+2=prime, 2 for p+2=semiprime. Potentially slow due to factorization.
"""
    if sympy.isprime(p+2):
        return 1
    f = sympy.factorint(p+2)
    return sum([f[ff] for ff in f]) == 2

##### Circular ###################################################

def circular (p, tout=0, stor=0):
    """
A circular prime is a prime number with the property that the number generated at each intermediate step when cyclically permuting its (base 10) digits will be prime.
"""
    sp = str(p)
    
    if len(sp) == 1:
        return True
    if "0" in sp or "2" in sp or "4" in sp or "5" in sp or "6" in sp or "8" in sp:
        return False
    cp = sp[1:]+sp[0]
    while cp != sp:
        icp = int(cp)
        if not sympy.isprime(icp):
            return False
        cp = cp[1:]+cp[0]
    return True

##### Cluster ###################################################

def cluster(p, tout=0, stor=0):
    """
In number theory, a cluster prime is a prime number p such that every even positive integer k ≤ p − 3 can be written as the difference between two prime numbers not exceeding p.

Potentially slow due to looping over range (2, p-2,2 ) and needing all primes <= p.
"""
    if p == 2:
        return False
    pm3 = p-3
    if stor and (p/log(p) > stor):
        return None
    listofprimes = list(sympy.primerange(p+1))
    time0 = process_time()
    for k in range (2, pm3+1, 2):
        if tout and (process_time()-time0 > tout):
            return None
        found = False
        for p1 in listofprimes:
            p2 = p1 - k
            if sympy.isprime(p2):
                found = True
                break
        if not found:
            return False
    return True

##### Cousin ###################################################

def cousin(p, tout=0, stor=0):
    """
In number theory, cousin primes are prime numbers that differ by four. 

Returns False, or tuplet of p's cousins.
"""
    ret = ()
    if p > 5 and sympy.isprime(p-4):
        ret += (p-4,)
    if sympy.isprime(p+4):
        ret += (p+4,)
    return False if ret == () else ret

##### Cuban ###################################################

def cuban(p, tout=0, stor=0):
    """
A cuban prime is a prime number that is also a solution to one of two different specific equations involving differences between third powers of two integers x and y. 

Returns False, or 1 or 2 indicating if type 1 or type 2.
"""
    # Type 1 is of form p = 3y^2+3y+1.
    # Has integer solution if (4p-1)/3 is a square.

    if (4*p-1)%3 == 0 and (isqrt((4*p-1)//3)**2) == (4*p-1)//3:
        return 1

    # Type 2 is of form p = 3y^2+6y+4.
    # Has integer solution if 4(p-1)/3 is a square.

    if 4*(p-1)%3 == 0 and (isqrt(4*(p-1)//3)**2) == 4*(p-1)//3:
        return 2
    
    return False

##### Cullen ###################################################

def cullen(p, tout=0, stor=0):
    """
In mathematics, a Cullen number is a member of the integer sequence {\displaystyle C_{n}=n\cdot 2^{n}+1}
"""
    for n in [1, 141, 4713, 5795, 6611, 18496, 32292, 32469, 59656, 90825, 262419, 361275, 481899, 1354828, 6328548, 6679881]:
        if p < n*2**n+1:
            return False
        if p == n*2**n+1:
            return True
    return None

    
##### Delicate ###################################################

def delicate(p, tout=0, stor=0):
    """
A delicate prime, digitally delicate prime, or weakly prime number is a prime number where, under a given radix but generally decimal, replacing any one of its digits with any other digit always results in a composite number.
"""
    ps = str(p)
    for i in range(len(ps)):
        for d in "0123456789":
            if ps[i] != d:
                pc = int(ps[:i]+d+ps[i+1:])
                if sympy.isprime(pc):
                    return False
    return True

##### Dihedral ###################################################

def dihedral(p, tout=0, stor=0):
    """
A dihedral prime or dihedral calculator prime is a prime number that still reads like itself or another prime number when read in a seven-segment display, regardless of orientation (normally or upside down), and surface (actual display or reflection on a mirror).
"""
    ps = str(p)
    if "3" in ps or "4" in ps or "6" in ps or "7" in ps or "9" in ps:
        return False
    upside = [0, 1, 2, -1, -1, 5, -1, -1, 8, -1]
    mirror = [0, 1, 5, -1, -1, 2, -1, -1, 8, -1]
    u = 0
    m = 0
    um = 0
    for i in range(len(ps)):
        psi = int(ps[i])
        psii = int(ps[-i-1])
        u = u*10 + upside[psii]
        m = m*10 + mirror[psii]
        um = um*10 + upside[mirror[psi]]
    return sympy.isprime(u) and sympy.isprime(m) and sympy.isprime(um)

##### Double Mersenne ###################################################

def dbmer(p, tout=0, stor=0):
    """
In mathematics, a double Mersenne number is a Mersenne number of the form

{\displaystyle M_{M_{p}}=2^{2^{p}-1}-1}

where p is prime. 
"""
    return class_from_list (p, [7, 127, 2147483647, 170141183460469231731687303715884105727], False, None)

##### Emirp ###################################################

def emirp(p, tout=0, stor=0):
    """
An emirp (prime spelled backwards) is a prime number that results in a different prime when its decimal digits are reversed.
"""
    ps = str(p)
    sp = ps[::-1]
    return (sp != ps) and (sympy.isprime(int(sp)))

##### Even ###################################################

def even(p, tout=0, stor=0):
    """
Primes of the form 2n.
"""
    return p == 2

##### Factorial ###################################################

facts = [1]
def factorial(p, tout=0, stor=0):
    """
A factorial prime is a prime number that is one less or one more than a factorial

Returns -1 or +1 or False.
"""
    global facts
    time0 = process_time()
    while facts[-1] < p+1:
        if stor and (len(facts) > stor):
            return None
        if tout and (process_time()-time0 > tout):
            return None
        facts.append(facts[-1] * (len(facts)+1))
    return 1 if (p-1 in facts) else -1 if (p+1 in facts) else False

##### Fermat ###################################################

def fermat(p, tout=0, stor=0):
    """
In mathematics, a Fermat number, named after Pierre de Fermat, the first known to have studied them, is a positive integer of the form

    {\displaystyle F_{n}=2^{2^{n}}+1,}...

If 2^k + 1 is prime and k > 0, then k itself must be a power of 2, so 2^k + 1 is a Fermat number; such primes are called Fermat primes. As of 2023, the only known Fermat primes are F0 = 3, F1 = 5, F2 = 17, F3 = 257, and F4 = 65537 (sequence A019434 in the OEIS); heuristics suggest that there are no more. 
"""
    return class_from_list (p, [3, 5, 17, 257, 65537], False, 2**4096+1)

##### Fibo ###################################################

def fibo(p, tout=0, stor=0):
    """
A Fibonacci prime is a Fibonacci number that is prime
"""
    # Test from https://www.baeldung.com/cs/test-fibonacci-number

    a1 = 5*p**2+4
    a2 = a1-8
    return (isqrt(a1))**2 == a1 or (isqrt(a2))**2 == a2

##### Fortunate ###################################################

forts=[3]
def fortunate(p, tout=0, stor=0, maxp=1000):
    """
A Fortunate number, named after Reo Fortune, is the smallest integer m > 1 such that, for a given positive integer n, pn# + m is a prime number

This involves sympy.nextprime(pn#) so gets very slow. Return `None` if `p` > `maxp`.
"""
    if p > maxp:
        return None
    time0 = process_time()
    while sympy.prime(len(forts)) < p:
        if stor and (len(forts) > stor):
            return None
        if tout and (process_time()-time0 > tout):
            return None
        n = len(forts)+1
        pn = sympy.primorial(n)
        forts.append(sympy.nextprime(pn+n)-pn)
    return class_from_list (p, forts, False, None)

##### Good ###################################################

def good(p, tout=0, stor=0):
    """
    A good prime is a prime number whose square is greater than the product of any two primes at the same number of positions before and after it in the sequence of primes.
"""
    if p == 2:
        return
    time0 = process_time()
    p2 = p**2
    pp = sympy.prevprime(p)
    np = sympy.nextprime(p)
    while True:
        if tout and (process_time()-time0 > tout):
            return None
        if p2 < pp*np:
            return False
        if pp == 2:
            return True
        pp = sympy.prevprime(pp)
        np = sympy.nextprime(np)
        
##### Happy ###################################################

def happy(p, tout=0, stor=0):
    """
In number theory, a happy number is a number which eventually reaches 1 when replaced by the sum of the square of each digit.
"""

    unhappy = [4, 16, 37, 58, 89, 145, 42, 20]
    ph = p
    time0 = process_time()
    while True:
        if tout and (process_time()-time0 > tout):
            return None
        phs = str(ph)
        ss = sum([int(pi)**2 for pi in phs])
        if ss == 1:
            return True
        if ss in unhappy:
            return False
        ph = ss
    return False

##### Higgs ###################################################

higgss = []
def higgs(p, expt=2, tout=0, stor=0):
    """
A Higgs prime, named after Denis Higgs, is a prime number with a totient (one less than the prime) that evenly divides the square of the product of the smaller Higgs primes. (This can be generalized to cubes, fourth powers, etc.)
"""
    while len(higgss) < expt-1:
        higgss.append([])
    if higgss[expt-2] == []:
        higgss[expt-2] = [2]
    nstore = sum(len(h) for h in higgss)
    time0 = process_time()
    if higgss[expt-2][-1] < p:
        sqp = prod(higgss[expt-2])**expt
        while higgss[expt-2][-1] < p:
            pn = higgss[expt-2][-1]
            while True:
                if stor and (nstore > stor):
                    return None
                if tout and (process_time()-time0 > tout):
                    return None
                pn = sympy.nextprime(pn)
                if sqp%(pn-1) == 0:
                    higgss[expt-2].append(pn)
                    nstore += 1
                    sqp *= pn**expt
                    break
    return class_from_list (p, higgss[expt-2], False, None)
    
##### Left-and-right-truncatable ###################################################

def lartrunc(p, tout=0, stor=0):
    """
A left-and-right-truncatable prime is a prime which remains prime if the leading ("left") and last ("right") digits are simultaneously successively removed down to a one- or two-digit prime. 
"""
    ps = str(p)[1:-1]
    while ps != "":
        if not sympy.isprime(int(ps)):
            return False
        ps = ps[1:-1]
    return True

##### Left-truncatable ###################################################
        
def ltrunc(p, tout=0, stor=0):
    """
In number theory, a left-truncatable prime is a prime number which, in a given base, contains no 0, and if the leading ("left") digit is successively removed, then all resulting numbers are prime.
"""
    ps = str(p)[1:]
    if "0" in ps:
        return False
    while ps != "":
        if not sympy.isprime(int(ps)):
            return False
        ps = ps[1:]
    return True

##### Lucas ###################################################

def lucas(p, tout=0, stor=0):
    """
A Lucas prime is a Lucas number that is prime
"""

    # Test from https://codegolf.stackexchange.com/questions/76835/is-this-a-lucas-number

    a1 = 5*p**2+20
    a2 = a1-40
    return (isqrt(a1))**2 == a1 or (isqrt(a2))**2 == a2

##### Mersenne ###################################################

mers = [[2], [3]] # [[primes], [Mersennes]]
def mer(p, tout=0, stor=0):
    """
In mathematics, a Mersenne prime is a prime number that is one less than a power of two.
"""
    return class_from_list (p, [3, 7, 31, 127, 8191, 131071, 524287, 2147483647, 2305843009213693951, 618970019642690137449562111, 162259276829213363391578010288127, 170141183460469231731687303715884105727], False, None)

##### Mills ###################################################

millss = [2]
def mills(p, tout=0, stor=0):
    """
 In number theory, Mills' constant is defined as the smallest positive real number A such that the floor function of the double exponential function

    \lfloor A^{3^{n}} \rfloor

is a prime number for all positive natural numbers n.
"""
    return class_from_list (p, [2, 11, 1361, 2521008887, 16022236204009818131831320183, 4113101149215104800030529537915953170486139623539759933135949994882770404074832568499], False, None)

##### Minimal ###################################################

def minimal(p, tout=0, stor=0):
    """
 In recreational number theory, a minimal prime is a prime number for which there is no shorter subsequence of its digits in a given base that form a prime. 
"""
    return class_from_list (p, [2, 3, 5, 7, 11, 19, 41, 61, 89, 409, 449, 499, 881, 991, 6469, 6949, 9001, 9049, 9649, 9949, 60649, 666649, 946669, 60000049, 66000049, 66600049], True)

##### Motzkin ###################################################

motzs = [1, 1]
def motzkin(p, tout=0, stor=0):
    """
In mathematics, the nth Motzkin number is the number of different ways of drawing non-intersecting chords between n points on a circle (not necessarily touching every point by a chord).
"""
    return class_from_list (p, [2, 127, 15511, 953467954114363], False, None)

##### Non-insertable ############################################

def nonins(p, tout=0, stor=0):
    """
A non-insertable prime is a prime with property that no matter where you insert (or prepend 
or append) a digit you get a composite number (except for prepending a zero). 
"""
    s = str(p)
    for c in "1379":
        if sympy.isprime(int(s+c)): return False
    for k in range(1,len(s)):
        s1 = s[:-k]
        s2 = s[-k:]
        for c in "0123456789":
            if sympy.isprime(int(s1+c+s2)): return False
    for c in "123456789":
        if sympy.isprime(int(c+s)): return False
    return True

##### Newman-Shanks-Williams ###################################################

def nsw(p, tout=0, stor=0):
    """
In mathematics, a Newman–Shanks–Williams prime (NSW prime) is a prime number p which can be written in the form

    S_{2m+1}=\frac{\left(1 + \sqrt{2}\right)^{2m+1} + \left(1 - \sqrt{2}\right)^{2m+1}}{2}.
"""
    return class_from_list (p, [7, 41, 239, 9369319, 63018038201, 489133282872437279, 19175002942688032928599, 123426017006182806728593424683999798008235734137469123231828679], False, None)

##### Palindromic ###################################################

def pal(p, tout=0, stor=0):
    """
In mathematics, a palindromic prime (sometimes called a palprime[1]) is a prime number that is also a palindromic number. 
"""
    ps = str(p)
    sp = ps[::-1]
    return (sp == ps)

##### Pell ###################################################

def pell(p, tout=0, stor=0):
    """
In mathematics, the Pell numbers are an infinite sequence of integers, known since ancient times, that comprise the denominators of the closest rational approximations to the square root of 2.
"""
    return class_from_list (p, [2, 5, 29, 5741, 33461, 44560482149, 1746860020068409, 68480406462161287469, 13558774610046711780701, 4125636888562548868221559797461449, 4760981394323203445293052612223893281], False, None)

##### Pell-Lucas ###################################################

def pelllucas(p, tout=0, stor=0):
    """
The numerators of the closest rational approximations to the square root of 2 are half the companion Pell numbers or Pell–Lucas numbers
"""
    return class_from_list (p, [3, 7, 17, 41, 239, 577, 665857, 9369319, 63018038201, 489133282872437279, 19175002942688032928599, 123426017006182806728593424683999798008235734137469123231828679], False, None)

##### Permutable ###################################################

def permutable(p, tout=0, stor=0):
    """
A permutable prime, also known as anagrammatic prime, is a prime number which, in a given base, can have its digits' positions switched through any permutation and still be a prime number.
"""
    return class_from_list (p, [2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97, 113, 131, 199, 311, 337, 373, 733, 919, 991, (10**19-1)//9, (10**23-1)//9, (10**317-1)//9, (10**1031-1)//9], False, None)
             
##### Pierpont ###################################################

def pierpont(p, tout=0, stor=0):
    """
In number theory, a Pierpont prime is a prime number of the form
{\displaystyle 2^{u}\cdot 3^{v}+1\,}
for some nonnegative integers u and v.
"""
    pm1 = p-1
    time0 = process_time()
    while pm1%2 == 0:
        if tout and (process_time()-time0 > tout):
            return None
        pm1 //= 2
    while pm1%3 == 0:
        if tout and (process_time()-time0 > tout):
            return None
        pm1 //= 3
    return pm1 == 1

##### Pillai ###################################################

def pillai(p, tout=0, stor=0):
    """
In number theory, a Pillai prime is a prime number p for which there is an integer n > 0 such that the factorial of n is one less than a multiple of the prime, but the prime is not one more than a multiple of n.
"""
    nf = 1
    time0 = process_time()
    for n in range (2, p):
        if tout and (process_time()-time0 > tout):
            return None
        nf *= n
        if (nf+1)%p == 0 and (p-1)%n != 0:
            return True
    return False

##### Prime quadruplet ###################################################

def primequadruplet(p, tout=0, stor=0):
    """
In number theory, a prime quadruplet (sometimes called prime quadruple) is a set of four prime numbers of the form {p, p + 2, p + 6, p + 8}.
Returns False or tuple of triples of other three members of quadruplets.
"""
    ret = ()
    if p == 5:
        ret += ((7, 11, 13),)
    if p == 7:
        ret += ((5, 11, 13),)
    if p == 11:
        ret += ((5, 7, 13),)
    if p == 13:
        ret += ((5, 7, 11),)
    if p%30 == 11:
        if sympy.isprime(p+2) and sympy.isprime(p+6) and sympy.isprime(p+8):
            ret += ((p+2, p+6, p+8),) 
    if p%30 == 13:
        if sympy.isprime(p-2) and sympy.isprime(p+4) and sympy.isprime(p+6):
            ret += ((p-2, p+4, p+6),) 
    if p%30 == 17:
        if sympy.isprime(p-6) and sympy.isprime(p-4) and sympy.isprime(p+2):
            ret += ((p-6, p-4, p+2),) 
    if p%30 == 19:
        if sympy.isprime(p-8) and sympy.isprime(p-6) and sympy.isprime(p-2):
            ret += ((p-8, p-6, p-2),) 
    return False if ret==() else ret


##### Prime triplet ###################################################

def primetriplet(p, tout=0, stor=0):
    """
In number theory, a prime triplet is a set of three prime numbers in which the smallest and largest of the three differ by 6.

Returns False or tuple of duples of other two members of triplets. 
"""
    ret = ()
    pm6 = sympy.isprime(p-6)
    pm2 = sympy.isprime(p-2)
    pm4 = sympy.isprime(p-4)
    p2 = sympy.isprime(p+2)
    p4 = sympy.isprime(p+4)
    p6 = sympy.isprime(p+6)
    if pm6 and pm4:
        ret += ((p-6, p-4),)
    if pm6 and pm2:
        ret += ((p-6, p-2),)
    if pm4 and p2:
        ret += ((p-4, p+2),)
    if pm2 and p4:
        ret += ((p-2, p+4),)
    if p2 and p6:
        ret += ((p+2, p+6),)
    if p4 and p6:
        ret += ((p+4, p+6),)
    return False if ret == () else ret
    
##### Primorial ###################################################

def primorial(p, tout=0, stor=0):
    """
In mathematics, a primorial prime is a prime number of the form pn# ± 1, where pn# is the primorial of pn (i.e. the product of the first n primes).
"""
    return class_from_list (p, [2, 3, 5, 7, 29, 31, 211, 2309, 2311, 30029, 200560490131, 304250263527209, 23768741896345550770650537601358309], False, None)

##### Proth ###################################################

def proth(p, tout=0, stor=0):
    """
A Proth number is a natural number N of the form {\displaystyle N=k\times 2^{n}+1} where k and n are positive integers, k is odd and 2^n > k.
"""
    pm1 = p-1
    twok = 1
    time0 = process_time()
    while pm1%2 == 0:
        if tout and (process_time()-time0 > tout):
            return None
        pm1 //= 2
        twok *= 2
    return twok > pm1

##### Pythagorean ###################################################

def pyth(p, tout=0, stor=0):
    """
A Pythagorean prime is a prime number of the form 4n+1.
"""
    return (p-1)%4 == 0

##### Quartan ###################################################

def quartan(p, tout=0, stor=0):
    """
In mathematics, a quartan prime is a prime number of the form x4 + y4 where x and y are positive integers.

Returns (x, y).
"""
    if p == 2:
        return (1, 1)
    if p%16 != 1:
        return False
    mxy = isqrt(isqrt(p))
    time0 = process_time()
    for x in range (1, mxy+1):
        if tout and (process_time()-time0 > tout):
            return None
        x4 = x**4
        for y in range (1, isqrt(isqrt(p-x4))+1):
            if x4+y**4 == p:
                return True
    return False    
    
##### Repunit ###################################################

def repu(p, tout=0, stor=0):
    """
In recreational mathematics, a repunit is a number like 11, 111, or 1111 that contains only the digit 1.
"""
    ps = str(p)
    ps = ps.replace("1","")
    return ps==""

##### Right-truncatable ###################################################

def rtrunc(p, tout=0, stor=0):
    """
A right-truncatable prime is a prime which remains prime when the last ("right") digit is successively removed.
"""
    ps = str(p)[:-1]
    if "0" in ps or "2" in ps[1:] or "4" in ps[1:] or "5" in ps[1:] or "6" in ps[1:] or "8" in ps[1:]:
        return False
    while ps != "":
        if not sympy.isprime(int(ps)):
            return False
        ps = ps[:-1]
    return True

##### Safe ###################################################

def safe(p, tout=0, stor=0):
    """
The number 2p + 1 associated with a Sophie Germain prime is called a safe prime.
"""
    return p > 2 and sympy.isprime((p-1)//2)

##### Sexy ###################################################

def sexy(p, tout=0, stor=0):
    """
In number theory, sexy primes are prime numbers that differ by six.

Returns False or p's sexy partner.
"""
    ret = ()
    if p > 7 and sympy.isprime(p-6):
        ret += (p-6,)
    if sympy.isprime(p+6):
        ret += (p+6,)
    return False if ret == () else ret

##### Sophie Germain ###################################################

def sophie(p, tout=0, stor=0):
    """
In number theory, a prime number p is a Sophie Germain prime if 2p + 1 is also prime.
"""
    return sympy.isprime(2*p+1)

##### Strobogrammatic ###################################################

def strobe(p, tout=0, stor=0):
    """
A strobogrammatic number is a number whose numeral is rotationally symmetric, so that it appears the same when rotated 180 degrees.
"""
    ps = str(p)
    if "2" in ps or "3" in ps or "4" in ps or "5" in ps or "7" in ps:
        return False
    upside = [0, 1, -1, -1, -1, -1, 9, -1, 8, 6]
    u = 0
    for i in range(len(ps)):
        psii = int(ps[-i-1])
        u = u*10 + upside[psii]
    return u == p

##### Strong ###################################################

def strong(p, tout=0, stor=0):
    """
In number theory, a strong prime is a prime number that is greater than the arithmetic mean of the nearest prime above and below
"""
    if p == 2:
        return False
    pp = sympy.prevprime(p)
    np = sympy.nextprime(p)
    return p > (pp+np)/2

##### Super-prime ###################################################

def superprime(p, tout=0, stor=0):
    """
Super-prime numbers, also known as higher-order primes or prime-indexed primes (PIPs), are the subsequence of prime numbers that occupy prime-numbered positions within the sequence of all prime numbers. 
"""
    r = class_from_list(p, primeslist, False, None)
    if r:
        pi = primeslist.index(p)+1
        return pi in primeslist
    else:
        return r
    
##### Supersingular ###################################################

def supersing(p, tout=0, stor=0):
    """
In the mathematical branch of moonshine theory, a supersingular prime is a prime number that divides the order of the Monster group M
"""
    return class_from_list (p, [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 41, 47, 59, 71], True)

##### Twin ###################################################

def twin(p, tout=0, stor=0):
    """
In number theory, twin primes are prime numbers that differ by two.

Returns False or tuplet of p's twins.
"""
    ret = ()
    if p > 3 and sympy.isprime(p-2):
        ret += (p-2,)
    if sympy.isprime(p+2):
        ret += (p+2,)
    return False if ret == () else ret

##### Wagstaff ###################################################

def wagstaff(p, tout=0, stor=0):
    """
In number theory, a Wagstaff prime is a prime number of the form

    {\displaystyle {{2^{p}+1} \over 3}}

where p is an odd prime. 
"""
    return class_from_list (p, [3, 11, 43, 683, 2731, 43691, 174763, 2796203, 715827883, 2932031007403, 768614336404564651, 201487636602438195784363, 845100400152152934331135470251, 56713727820156410577229101238628035243, 62357403192785191176690552862561408838653121833643], False, None)

##### Wieferich ###################################################

def wief(p, tout=0, stor=0):
    """
In number theory, a Wieferich prime is a prime number p such that p^2 divides 2^(p−1) − 1
"""
    return class_from_list (p, [1093, 3511], False, 2**64)

##### Williams ###################################################

def williams(p, b=3, tout=0, stor=0):
    """
In number theory, a Williams number base b is a natural number of the form {\displaystyle (b-1)\cdot b^{n}-1} for integers b ≥ 2 and n ≥ 1.

Returns False or n.
"""
    if b == 2:
        return mers(p, tout, stor)
    pp1 = p+1
    if pp1%(b-1) != 0:
        return False
    n = 0
    bn = pp1//(b-1)
    time0 = process_time()
    while bn%b == 0:
        if tout and (process_time()-time0 > tout):
            return None
        bn //= b
        n += 1
    return n if bn == 1 else False

##### Wilson ###################################################

def wilson(p, tout=0, stor=0):
    """
In number theory, a Wilson prime is a prime number p such that p^{2} divides (p-1)!+1
"""
    
    return class_from_list (p, [5, 13, 563], False, 20000000000000)

##### Wolstenholme ###################################################

def wolsten(p, tout=0, stor=0):
    """
In number theory, a Wolstenholme prime is a special type of prime number satisfying a stronger version of Wolstenholme's theorem.
"""
    return class_from_list (p, [16843, 2124679], False, 1000000000)
    
##### Woodall ###################################################

def woodall(p, tout=0, stor=0):
    """
 In number theory, a Woodall number (Wn) is any natural number of the form

    W_{n}=n 2^{n}-1
"""
    return class_from_list (p, [7, 23, 383, 32212254719, 2833419889721787128217599, 195845982777569926302400511, 4776913109852041418248056622882488319, 1307960347852357218937346147315859062783, 225251798594466661409915431774713195745814267044878909733007331390393510002687], False, None)

##################################################################

def class_from_list (p, thelist, complete, limit=None):
    """
`thelist` is a list of members of the prime class
`complete` indicates whether that list is complete
`limit` (or `thelist[-1]+1 if `limit` is `None` is lower limit on next class member (or limit beyond which we don't care)

If p is in class according to these, return True; if not, return False; if don't know, return None.
"""

    if p in thelist:
        return True
    elif complete:
        return False
    else:
        l = thelist[-1]+1 if limit == None else limit
        return False if p < l else None

legal_extra_funcs = (higgs, williams) # functions that take extra argument
    
def describe(p, tout=0, stor=0, extras={higgs: (2,), williams: (3, 10)}):
    """
Call all the classification functions and return a list of ones that passed.
extras is a hash with entries like:
   {func: (a)} -- call func with added argument = a
   {func: (a, b)} -- same but for arguments in range (a, b)
   {func: (a, b, c)} -- same but for arguments in range (a, b, c)
"""
    def desc_list (fn, ex, c):
        if (fn[0] == higgs) and ex:
            return f"{fn[1]} exponent {ex}"
        if fn[0] == williams:
            if ex:
                return f"{fn[1]} base {ex} (n={c})"
            else:
                return f"{fn[1]} (n={c})"
        else:
            if fn[0] == cousin or fn[0] == primequadruplet or \
               fn[0] == primetriplet or fn[0] == sexy or fn[0] == twin:
                if len(c) == 1:
                    return f"{fn[1]} (with {c[0]})"
                elif len(c) == 2:
                    return f"{fn[1]} (with {c[0]} and {c[1]})"
                else:
                    return f"{fn[1]} (with {', '.join([str(ci) for ci in c[:-1]])+', and '+str(c[-1])})"
            elif fn[0] == cuban:
                return f"{fn[1]} (type {c})"
            else:
                return f"{fn[1]}"

    funcs = (
        (balanced, "balanced"),
        (chen, "Chen"),
        (circular, "circular"),
        (cluster, "cluster"),
        (cousin, "cousin"),
        (cuban, "cuban"),
        (cullen, "cullen"),
        (delicate, "delicate"),
        (dihedral, "dihedral"),
        (dbmer, "double Mersenne"),
        (emirp, "emirp"),
        (even, "even"),
        (factorial, "factorial"),
        (fermat, "Fermat"),
        (fibo, "Fibonacci"),
        (fortunate, "fortunate"),
        (good, "good"),
        (happy, "happy"),
        (higgs, "Higgs"),
        (lartrunc, "left-and-right-truncatable"),
        (ltrunc, "left-truncatable"),
        (lucas, "Lucas"),
        (mer, "Mersenne"),
        (mills, "Mills"),
        (minimal, "minimal"),
        (motzkin, "Motzkin"),
        (nonins, "non-insertable"),
        (nsw, "Newman–Shanks–Williams"),
        (pal, "palindromic"),
        (pell, "Pell"),
        (pelllucas, "Pell-Lucas"),
        (permutable, "permutable"),
        (pierpont, "Pierpont"),
        (pillai, "Pillai"),
        (primequadruplet, "prime quadruplet"),
        (primetriplet, "prime triplet"),
        (primorial, "primorial"),
        (proth, "Proth"),
        (pyth, "Pythagorean"),
        (quartan, "quartan"),
        (repu, "repunit"),
        (rtrunc, "right-truncatable"),
        (safe, "safe"),
        (sexy, "sexy"),
        (sophie, "Sophie Germain"),
        (strobe, "strobogrammatic"),
        (strong, "strong"),
        (superprime, "super-prime"),
        (supersing, "supersingular"),
        (twin, "twin"),
        (wagstaff, "Wagstaff"),
        (wief, "Wieferich"),
        (williams, "Williams"),
        (wilson, "Wilson"),
        (wolsten, "Wolstenholme"),
        (woodall, "Woodall")
        )

    if not sympy.isprime(p):
        return False

    list = []
    for fnt in funcs:
        if fnt[0] in extras:
            if not fnt[0] in legal_extra_funcs:
                print (f"{fnt[1]} has no additional arguments")
                return
            x0 = 0
            x1 = 1
            x2 = 1
            x = extras[fnt[0]]
            if len(x) >= 1:
                x0 = x[0]
                if len(x) == 1:
                    x1 = x0 + 1
                else:
                    x0 = x[0]
                    x1 = x[1]
                    if len(x) == 3:
                        x2 = x[2]
            for ex in range (x0, x1, x2):
                c = fnt[0](p, ex, tout=tout, stor=stor)
                if c:
                    list.append (desc_list (fnt, ex, c))
        else:
            c = fnt[0](p, tout=tout, stor=stor)
            if c:
                list.append (desc_list (fnt, None, c))
                
    return list

def test_classify (fn, limit1, limit2=-1, tout=0, stor=0, extra=None):
    # Call a classification function for a range of limits and print results
    # If `extra` and `fn` takes extra argument, use given value
    
    if extra and fn not in legal_extra_funcs:
        print ("Function takes no additional argument")
        return
    
    p = 2 if limit2==-1 else sympy.nextprime(limit1-1)
    lim = limit1 if limit2==-1 else limit2
    while p <= lim:
        if fn in legal_extra_funcs and extra != None:
            t = fn(p, extra, tout=tout, stor=stor)
        else:
            t = fn(p, tout=tout, stor=stor)
        if t:
            print (p, t)
        p = sympy.nextprime (p)
        
